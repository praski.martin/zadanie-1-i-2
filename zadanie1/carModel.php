<?php

interface EngineInterface
{
    public function powerUp();

    public function powerDown();

    public function on();

    public  function of();
}

abstract class EngineAbstract implements EngineInterface
{
    public function on()
    {
        echo 'silnik jest włączony' . PHP_EOL;
    }
    public function of()
    {
        echo 'silnik jest wyłączony' . PHP_EOL;
    }
}

class V8Engine extends EngineAbstract
{
    public function powerUp()
    {
        echo 'silnik generuje prędkość większą o 100 km/h' . PHP_EOL;
    }

    public function powerDown()
    {
        echo 'silnik generuje prędkość mniejszą o 20 km/h' . PHP_EOL;
    }
}

interface SteeringWheelInterface
{
    public function turnLeft();

    public function turnRight();
}

class SteeringWheel implements SteeringWheelInterface
{
    public function turnLeft()
    {
        echo 'pojazd skręca w lewo' . PHP_EOL;
    }

    public function turnRight()
    {
        echo 'pojazd skręca w prawo' . PHP_EOL;
    }
}

interface RadioInterface
{
    public function changeStation();

    public function on();

    public function of();
}

class Radio implements RadioInterface
{
    private const STATIONS = ['rmf' , 'classic' , 'jazz'];

    public function changeStation()
    {
        echo 'zmień stację radiową na ' . self::STATIONS[rand(0, count(self::STATIONS) - 1)] . PHP_EOL;
    }

    public function on()
    {
        echo 'włącz radio' . PHP_EOL;
    }

    public function of()
    {
        echo 'wyłącz radio' . PHP_EOL;
    }
}

interface VehicleInterface
{
    public function on();

    public function of();

    public function faster();

    public function slower();

    public function changeRadioStation();

    public function turnLeft();

    public function turnRight();
}

class Car implements VehicleInterface
{
    private $engine;
    private $steeringWheel;
    private $radio;

    public function __construct(EngineInterface $engine,SteeringWheelInterface $steeringWheel, RadioInterface $radio)
    {
        $this->engine = $engine;
        $this->steeringWheel = $steeringWheel;
        $this->radio = $radio;
    }

    public function on()
    {
        $this->engine->on();
        $this->radio->on();
    }

    public function of()
    {
        $this->radio->of();
        $this->engine->of();
    }

    public function faster()
    {
        $this->engine->powerUp();
    }

    public function slower()
    {
        $this->engine->powerDown();
    }

    public function changeRadioStation()
    {
        $this->radio->changeStation();
    }

    public function turnLeft()
    {
        $this->steeringWheel->turnLeft();
    }

    public function turnRight()
    {
        $this->steeringWheel->turnRight();
    }
}

class Trip
{
    public function goTrip(VehicleInterface $vehicle)
    {
        $vehicle->on();
        $vehicle->changeRadioStation();
        $vehicle->changeRadioStation();
        $vehicle->faster();
        $vehicle->turnLeft();
        $vehicle->slower();
        $vehicle->changeRadioStation();
        $vehicle->faster();
        $vehicle->turnRight();
        $vehicle->of();
    }
}

$car = new Car(new V8Engine(), new SteeringWheel(), new Radio());
$trip = new Trip;

$trip->goTrip($car);